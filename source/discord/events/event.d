/**
 * d-discord
 * A simple, easy to use Discord library for making bots written in 
 * the D programming language.
 * 
 * Author: Dhillon (dhilln)
 * License: See LICENSE for details.
**/

module discord.events.event;

import vibe.d;
import painlessjson;

/**
 * A generic event that we get from the gateway
**/
struct Event
{
    @SerializedName("t")
    string type;

    @SerializedName("d")
    JSONValue[string] data;

    @SerializedName("client_status")
    JSONValue[string] clientStatus;
}