/**
 * d-discord
 * A simple, easy to use Discord library for making bots written in 
 * the D programming language.
 * 
 * Author: Dhillon (dhilln)
 * License: See LICENSE for details.
**/

module discord.gateway;
// Phobos imports
import std.stdio;
// import std.json : JSONValue;
// import std.datetime;
import std.conv : to;

// Libs
import painlessjson;
import vibe.d;
// import vibe.http.websockets;
// import vibe.http.client;
// import vibe.core.log;
// import vibe.core.core;
// import vibe.core.net;

import discord.events.event;

/**
 * Discord opcodes for the Gateway
**/
enum GatewayOpcodes : int
{
    dispatch = 0,
    heartbeat = 1,
    identify = 2,
    statusUpdate = 3,
    resume = 6,
    reconnect = 7,
    requestGuildMembers = 8,
    invalidSession = 9,
    hello = 10,
    heartbeatAck = 11
}

/**
 * Gateway event names 
**/
enum GatewayEvents
{
    ready = "READY",
    typingStart = "TYPING_START",
    messageCreate = "MESSAGE_CREATE",
}

/**
 * Provides an interface for authorizating with and interacting with the
 * Discord gateway API and all of its endpoints.
**/
class Gateway
{
    import discord.endpoints : Endpoints;

    private {
        WebSocket _webSocket;
        int heartbeatInterval;
    }

    /**
     * Authenticates with the Discord gateway with the token.
     * Params:
     *  token = Token to authenticate using
    **/
    public void authenticate(string token)
    {
        logInfo("Making a GET request to the Discord HTTPS gateway..");

        // Make a request to the gateway
        auto response = requestHTTP(Endpoints.gateway, (scope req) {
            req.method = HTTPMethod.GET;
            req.headers["Content-Type"] = "application/json";
            req.headers["Authorization"] = "Bot " ~ token;
        });

        // Request succeeded
        if (response.statusCode == 200)
        {
            logInfo("All is good, now forwarding to the WebSocket gateway!");

            // Connect to the WebSocket
            connect(token);
        }
    }

    /**
     * Connect to the Discord WebSocket and start receiving messages.
     * Params: 
     *  token = Token to authenticate using
    **/
    public void connect(string token)
    {
        logInfo("Connecting to the WebSocket gateway..");

        // Connect to the WebSocket gateway
        _webSocket = connectWebSocket(URL(Endpoints.websocketGateway));
        sendIdentification(token);

        // Receive messages from the gateway
        runTask({ receiveMessages(); });

        // Heartbeat babumpbabump
        setTimer(40.seconds, toDelegate(&sendHeartbeat), true);
    }

    /**
     * Send a heartbeat every 40 seconds, sleeping between them.
     * This keeps the connection with Discord alive and allows us to continue.
    **/
    private void sendHeartbeat()
    {
        while (_webSocket.connected) 
        {
            logInfo("Sending a heartbeat to keep connection alive..");
            _webSocket.send(format(`{"op": %d, "d": null}`,
            GatewayOpcodes.heartbeat
            ));

            logInfo("Sleeping for 40 seconds before sending again..");
            sleep(40.seconds);
        }
    }

    /**
     * Send an identification payload containing the token
     * to identify ourself with the Discord gateway and gain access.
     * Params:
     *  token = Token to authenticate using
    **/
    private void sendIdentification(string token) 
    {
        string payload = format(`
        {
            "t": null, 
            "s": null,
            "op": %d,
            "d": 
            {
                "token": "%s",
                "properties":
                {
                    "$os": "d-discord",
                    "$browser": "d-discord",
                    "$device": "pc"
                }
            }
        }
        `, 
        GatewayOpcodes.identify, 
        token);

        logInfo(payload);
        if (!_webSocket.connected)
        {
            return;
        }

        _webSocket.send(payload);
    }

    /**
     * Process the dispatch message in Json format.
     * Params:
     *  jsonData = The JSON data to decode.
    **/
    private void processDispatchMessage(JSONValue json)
    {
        //string eventType = jsonData["t"].get!string;
        logInfo(json.toJSON.toString);
    }

    /**
     * vibe.d task to receive messages from the connected WebSocket
     * and parse and handle the content of them.
    **/
    private void receiveMessages()
    {
        while (_webSocket.connected)
        {
            // Return if we are not waiting for data
            if (!_webSocket.waitForData())
                return;

            // Decode the message
            int opcode;
            string rawJsonString;
            JSONValue jsonData;

            // There should always be an opcode, but to be safe
            try 
            {
                rawJsonString = _webSocket.receiveText;
                //jsonData = parseJsonString(_webSocket.receiveText);
                jsonData = toJSON(rawJsonString);
                writeln("toJSON "~jsonData.toString);
                string[string] data = fromJson!(string[string])(jsonData);
                writeln(data);
                //opcode = jsonData["op"].get!int;
            }
            catch (Exception ex)
            {
                logError("Error while decoding message: " ~ ex.message);
            }

            // For now just output the stuffs
            writefln("Opcode: %d", opcode);
            writefln("JSON: %s", rawJsonString);

            // Handle the opcode
            logInfo(opcode.stringof);
            switch (opcode)
            {
                // TODO: Add actual handlers for opcodes
                case GatewayOpcodes.dispatch:
                    processDispatchMessage(jsonData);
                    break;
                case GatewayOpcodes.heartbeat:
                    break;
                default:
                    break;  
            }
        }
    }
}