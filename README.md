# d-discord
A simple, easy to use Discord library for making bots written in the D programming language.

# Installation
To install make sure you have `vibe.d` installed, you can install it by adding it as a
dependency to `dub.sdl` or `dub.json`. Next you will need to make sure that `vibe.d` uses it's
`shared static this` as a main method, so add this to your `dub.sdl/json` as well.

```
name "my-project"
description "A minimal D application."
authors "Your Name"
copyright "Copyright © 2019, Your Name"
license "proprietary"

# very important, we need vibe
versions "VibeDefaultMain"
dependency "vibe-d" version="~>0.8.5"
```

# Usage
Currently, as of v0.1 there is no reason to actually use the API in your own projects
and as a result of this, no elegant wrappers are available. However if you **really** want to use it,
please use the below code example.

```d
import std.stdio;
import vibe.d; // very important, we need vibe

shared static this() 
{
    const string token = "your token here";

    // Create a new instance of the Gateway class.
    auto gateway = new Gateway();

    // Authenticate with your token
    gateway.authenticate(token);

    // Thats it!
}
```